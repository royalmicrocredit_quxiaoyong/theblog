package org.fantasizer.theblog.xo.mapper;

import org.fantasizer.common.mapper.TheBlogMapper;
import org.fantasizer.theblog.xo.entity.Visitor;

/**
 * @Author Cruise Qu
 * @Date 2020-01-30 19:38
 */
public interface VisitorMapper extends TheBlogMapper<Visitor> {
}
