package org.fantasizer.theblog.xo.mapper;

import org.fantasizer.common.mapper.TheBlogMapper;
import org.fantasizer.theblog.xo.entity.Menu;

/**
 * @Author Cruise Qu
 * @Date 2020-01-30 19:33
 */
public interface MenuMapper extends TheBlogMapper<Menu> {
}
