package org.fantasizer.theblog.xo.mapper;

import org.fantasizer.common.mapper.TheBlogMapper;
import org.fantasizer.theblog.xo.entity.Picture;

/**
 * @Author Cruise Qu
 * @Date 2020-01-30 19:35
 */
public interface PictureMapper extends TheBlogMapper<Picture> {
}
