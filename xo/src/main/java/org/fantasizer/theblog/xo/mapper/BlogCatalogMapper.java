package org.fantasizer.theblog.xo.mapper;

import org.fantasizer.common.mapper.TheBlogMapper;
import org.fantasizer.theblog.xo.entity.BlogCatalog;

/**
 * @Author Cruise Qu
 * @Date 2020-01-30 19:32
 */
public interface BlogCatalogMapper extends TheBlogMapper<BlogCatalog> {
}
