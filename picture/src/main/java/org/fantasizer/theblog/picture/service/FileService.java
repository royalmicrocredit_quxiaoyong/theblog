package org.fantasizer.theblog.picture.service;

import org.fantasizer.common.service.BaseService;
import org.fantasizer.theblog.picture.entity.File;

public interface FileService extends BaseService<File> {

}
