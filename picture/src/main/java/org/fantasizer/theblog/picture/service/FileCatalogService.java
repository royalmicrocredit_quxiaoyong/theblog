package org.fantasizer.theblog.picture.service;

import org.fantasizer.common.service.BaseService;
import org.fantasizer.theblog.picture.entity.FileCatalog;
public interface FileCatalogService extends BaseService<FileCatalog> {

}
