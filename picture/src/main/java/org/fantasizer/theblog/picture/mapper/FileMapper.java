package org.fantasizer.theblog.picture.mapper;

import org.fantasizer.common.mapper.TheBlogMapper;
import org.fantasizer.theblog.picture.entity.File;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author xuzhixiang
 * @since 2018-09-17
 */
public interface FileMapper extends TheBlogMapper<File> {

}
