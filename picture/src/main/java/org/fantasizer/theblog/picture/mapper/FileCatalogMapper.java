package org.fantasizer.theblog.picture.mapper;

import org.fantasizer.common.mapper.TheBlogMapper;
import org.fantasizer.theblog.picture.entity.FileCatalog;

public interface FileCatalogMapper extends TheBlogMapper<FileCatalog> {

}
